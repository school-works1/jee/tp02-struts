<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Compte</title>
  </head>
  <body>
  <h1>Compte d'utilisateur</h1>
    <s:form action="confcompte">
      <s:textfield name="compte.nom" label="nom" />
      <s:textfield name="compte.prenom" label="prenom" />
      <s:textfield name="compte.courrier" label="courrier" />
      <s:textfield name="compte.password" label="mot de passe" />
      <s:textfield name="compte.confPassword" label="confirmer mot de passe" />
      <s:submit value="creer un compte" />
      <s:reset value="rest" />
    </s:form>

  </body>
</html>
