<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
  <meta charset="UTF-8">
  <title>Confirmation d'inscription</title>
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet"/>

</head>
<body>
<div class="rounded bg-white-400 shadow-lg inline-block p-8 m-8">
  <h1 class="text-center text-blue-800">Confirmation de inscription</h1>
  <div class="flex flex-col p-4">
    <span class="text-blue-400">Nom</span>
    <span class="font-light"><s:property value="laureat.nom"/></span>
  </div>
  <div class="flex flex-col p-4">
    <span class="text-blue-400">Prenom</span>
    <span class="font-light"><s:property value="laureat.prenom"/></span>
  </div>
  <div class="flex flex-col p-4">
    <span class="text-blue-400">Courrier</span>
    <span class="font-light"><s:property value="laureat.email"/></span>
  </div>
  <div class="flex flex-col p-4">
    <span class="text-blue-400">telephone</span>
    <span class="font-light"><s:property value="laureat.telephone"/></span>
  </div>
  <div class="flex flex-col p-4">
    <span class="text-blue-400">entreprise</span>
    <span class="font-light"><s:property value="laureat.entreprise"/></span>
  </div>
  <input type="button" value="Confirmer" class="bg-blue-400 p-2 text-white bg:hover-blue-300"/>
</div>

</body>
</html>
