package action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.CompositeTextProvider;
import model.Compte;

public class ConfCompteAction extends ActionSupport {

    private Compte compte;

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }
}
